"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
require("mocha");
const Call_1 = require("../src/Logic/Call/Models/Call");
const ArchiveCallUseCase_1 = require("../src/Logic/Call/UseCases/ArchiveCallUseCase");
describe('Archive Call Use Case', () => {
    it('should mark the call as archived', () => {
        let call = new Call_1.Call(1);
        chai_1.expect(call.Archived).to.equal(false);
        call = new ArchiveCallUseCase_1.ArchiveCallUseCase().execute(call);
        chai_1.expect(call.Archived).to.equal(true);
    });
});
