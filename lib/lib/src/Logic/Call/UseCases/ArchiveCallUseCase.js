"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ArchiveCallUseCase {
    execute(call) {
        call.Archived = true;
        return call;
    }
}
exports.ArchiveCallUseCase = ArchiveCallUseCase;
