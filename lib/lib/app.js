"use strict";
const mysql = require('mysql');
const connection = mysql.createConnection({
    user: 'linergy',
    password: 'linergy',
    database: 'linergy',
    port: 8312
});
connection.connect((err) => {
    if (err) {
        console.log('Error connecting to Db');
        return;
    }
    console.log('Connection established');
});
connection.end((err) => {
    // The connection is terminated gracefully
    // Ensures all previously enqueued queries are still
    // before sending a COM_QUIT packet to the MySQL server.
});
