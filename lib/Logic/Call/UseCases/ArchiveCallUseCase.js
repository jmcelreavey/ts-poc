"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ArchiveCallUseCase {
    execute(call) {
        call.Archived = true;
        call.save();
        return call;
    }
}
exports.ArchiveCallUseCase = ArchiveCallUseCase;
