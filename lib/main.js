"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ArchiveCallUseCase_1 = require("./Logic/Call/UseCases/ArchiveCallUseCase");
const sequelize_typescript_1 = require("sequelize-typescript");
const Call_1 = require("./Models/Call");
const sequelize = new sequelize_typescript_1.Sequelize({
    database: 'linergy',
    username: 'linergy',
    password: 'linergy',
    port: 8312,
    modelPaths: [__dirname + '/Models/**/*'],
    host: '127.0.0.1',
    dialect: "mysql"
});
Call_1.Call.findByPk(1).then(call => {
    console.log(call);
    call = new ArchiveCallUseCase_1.ArchiveCallUseCase().execute(call);
    console.log(call);
});
