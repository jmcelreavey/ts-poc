"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ArchiveCallUseCase_1 = require("./Logic/Call/UseCases/ArchiveCallUseCase");
const Call_1 = require("./Logic/Call/Models/Call");
let call = new Call_1.Call(1);
console.log(call);
call = new ArchiveCallUseCase_1.ArchiveCallUseCase().execute(call);
console.log(call);
