import { expect } from 'chai';
import 'mocha';
import { ArchiveCallUseCase } from '../src/Logic/Call/UseCases/ArchiveCallUseCase';
import { Call } from '../src/Models/Call';

describe('Archive Call Use Case', () => {

  it('should mark the call as archived', () => {
      let call = new Call();

      expect(call.Archived).to.equal(false);

      call = new ArchiveCallUseCase().execute(call);

      expect(call.Archived).to.equal(true);
  });

});