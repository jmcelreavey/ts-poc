import {Table, Column, Model} from 'sequelize-typescript';

@Table({
    tableName: 'tblCall',
  })
export class Call extends Model<Call> {

    @Column
    Archived: boolean = false;
}