import {Call} from '../../../Models/Call';
import { IUseCase } from '../../Base/IUseCase';

export class ArchiveCallUseCase implements IUseCase {
    public execute(call : Call | null) {
        if (call) {
            call.Archived = true;
            call.save();
        }
        return call;
    }
}